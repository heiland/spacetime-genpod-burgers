import numpy as np


def substractoverhead(walltime, overhead):
    print '   \\texttt{{{0:.4f}}}'.format(walltime[0]-overhead[0])
    for kk in range(walltime.size-1):
        print '&  \\texttt{{{0:.4f}}}'.format(walltime[kk+1]-overhead[kk+1])
    print '\\\\'

print '# ## Heart Shape'
print '### we check hqhs -- nu=0.005, alpha=6.25e-05, N=200, tol=0.0001'
print '# $ 13$ & $ 18$ \\ $ 15$ & $ 19$ \\ $ 16$ & $ 20$ \\ $ 19$ & $ 15$ \\ $ 20$ & $ 16$ \\ $ 18$ & $ 13$'
wts = np.array([17.7408, 22.0753, 31.0965, 14.0667, 27.1249, 11.1985])
ovh = np.array([4.5106, 5.1961, 5.8363, 3.2786, 5.4045, 2.8650])
substractoverhead(wts, ovh)

print '### we check hqhs -- nu=0.005, alpha=6.25e-05, N=200, tol=0.0001'
print '$ 10$ & $ 10$ \\ $ 12$ & $ 12$ \\ $ 15$ & $ 15$ \\ $ 18$ & $ 18$ \\ $ 21$ & $ 21$ \\ $ 25$ & $ 25$'
wts = np.array([7.8848, 10.0882, 10.1934, 24.6438, 97.1266, 258.6737])
ovh = np.array([2.1390, 2.5895, 2.5863, 5.5441, 8.7651, 8.5990])
substractoverhead(wts, ovh)

print '### we check nu -- alpha=6.25e-05, N=200, tol=0.0001, (hq,hs)=(18,18)'
print '$5.00e-04$ & $1.00e-03$ & $2.00e-03$ & $4.00e-03$ & $8.00e-03$ & $1.60e-02$ & $3.20e-02$'
wts = np.array([56.9570, 27.9522, 27.6620, 29.0204, 24.8409, 45.0421, 48.3320])
ovh = np.array([14.5403, 6.7065, 6.2883, 6.5419, 5.1120, 13.5139, 13.0954])
substractoverhead(wts, ovh)

print '### we check nu -- alpha=6.25e-05, N=200, tol=0.0001, (hq,hs)=(13,18)'
print '$5.00e-04$ & $1.00e-03$ & $2.00e-03$ & $4.00e-03$ & $8.00e-03$ & $1.60e-02$ & $3.20e-02$'
wts = np.array([17.2110, 21.6937, 14.3517, 17.1875, 15.4581, 24.3240, 26.2829])
ovh = np.array([4.2996, 5.6909, 3.4428, 4.2857, 3.8199, 5.9091, 6.9617])
substractoverhead(wts, ovh)

print '### we check alpha -- nu=0.005, N=200, tol=0.0001, (hq,hs)=(18,18)'
print '$7.81e-06$ & $1.56e-05$ & $3.13e-05$ & $6.25e-05$ & $1.25e-04$ & $2.50e-04$'
wts = np.array([28.0792, 20.4735, 24.3930, 24.6179, 25.2003, 18.7734])
ovh = np.array([5.9868, 4.2727, 5.5816, 5.4987, 5.6456, 4.0777])
substractoverhead(wts, ovh)

print '### we check alpha -- nu=0.005, N=200, tol=0.0001, (hq,hs)=(13,18)'
print '$7.81e-06$ & $1.56e-05$ & $3.13e-05$ & $6.25e-05$ & $1.25e-04$ & $2.50e-04$'
wts = np.array([17.8579, 20.2696, 13.8128, 17.7508, 18.0643, 11.4755])
ovh = np.array([4.4755, 5.1431, 3.4178, 4.5134, 4.5614, 2.6705])
substractoverhead(wts, ovh)

print '### we check gtol -- nu=0.005, alpha=6.25e-05, hqhs=18-18, tol=0.0001'
print '$3.54e-04$ & $5.00e-04$ & $7.07e-04$ & $1.00e-03$ & $1.41e-03$ & $2.00e-03$'
wts = np.array([24.6704, 19.6084, 13.2889, 11.6492, 7.4211, 6.7471])
ovh = np.array([5.5484, 4.1406, 2.6504, 2.2052, 1.4148, 1.3036])
substractoverhead(wts, ovh)

print '### we check gtol -- nu=0.005, alpha=6.25e-05, hqhs=13-18, tol=0.0001'
print '$3.54e-04$ & $5.00e-04$ & $7.07e-04$ & $1.00e-03$ & $1.41e-03$ & $2.00e-03$'
wts = np.array([17.6740, 15.2859, 8.5706, 6.1516, 4.8607, 4.4731])
ovh = np.array([4.4763, 4.0471, 2.2770, 1.4557, 1.0744, 0.9565])
substractoverhead(wts, ovh)

print '# ## INIVAL ## #'
print '### we check hqhs -- nu=0.005, alpha=6.25e-05, N=200, tol=0.0001'
print '$ 10$ & $ 10$ \\ $ 12$ & $ 12$ \\ $ 15$ & $ 15$ \\ $ 18$ & $ 18$ \\ $ 21$ & $ 21$ \\ $ 25$ & $ 25$'
wts = np.array([6.6022, 8.3748, 14.8298, 27.4602, 76.0402, 206.9003])
ovh = np.array([2.0739, 2.5145, 4.4502, 6.3581, 4.4023, 5.4046])
substractoverhead(wts, ovh)

print '### we check hqhs -- nu=0.005, alpha=6.25e-05, N=200, tol=0.0001'
print '# $ 13,  18$ \\ $ 15,  19$ \\ $ 16,  20$ \\ $ 19,  15$ \\ $ 20,  16$ \\ $ 18,  13$'
wts = np.array([10.0038, 17.1761, 24.3549, 21.4312, 28.1523, 16.8206])
ovh = np.array([2.7075, 3.9602, 4.2998, 5.6657, 5.2374, 5.1880])
substractoverhead(wts, ovh)

print '### we check nu -- alpha=6.25e-05, N=200, tol=0.0001, (hq,hs)=(13,18)'
print '$5.00e-04, 1.00e-03, 2.00e-03, 4.00e-03, 8.00e-03, 1.60e-02, 3.20e-02$'
wts = np.array([19.9862, 18.5940, 15.4784, 15.1096, 20.6588, 25.7238, 32.6637])
ovh = np.array([7.0171, 6.8811, 5.1600, 4.6264, 5.1199, 8.3241, 9.6503])
substractoverhead(wts, ovh)

print '### we check nu -- alpha=6.25e-05, N=200, tol=0.0001, (hq,hs)=(18,18)'
print '$5.00e-04, 1.00e-03, 2.00e-03, 4.00e-03, 8.00e-03, 1.60e-02, 3.20e-02$'
wts = np.array([29.4322, 22.5139, 17.5741, 21.9100, 27.0386, 29.0167, 22.2037])
ovh = np.array([8.0400, 5.8867, 4.2575, 4.8668, 5.1663, 8.4557, 5.0920])
substractoverhead(wts, ovh)

print '### we check alpha -- nu=0.005, N=200, tol=0.0001, (hq,hs)=(18,18)'
print '$7.81e-06, 1.56e-05, 3.13e-05, 6.25e-05, 1.25e-04, 2.50e-04$'
wts = np.array([22.6808, 18.5661, 28.2693, 27.4127, 19.1700, 18.3394])
ovh = np.array([5.0654, 4.0251, 6.8534, 6.3728, 4.5589, 4.3392])
substractoverhead(wts, ovh)

print '### we check alpha -- nu=0.005, N=200, tol=0.0001, (hq,hs)=(13,18)'
print '$7.81e-06, 1.56e-05, 3.13e-05, 6.25e-05, 1.25e-04, 2.50e-04$'
wts = np.array([16.4638, 16.8308, 16.7133, 16.8609, 17.9836, 18.1794])
ovh = np.array([5.0497, 5.0894, 5.0943, 5.1815, 5.7123, 5.5778])
substractoverhead(wts, ovh)

print '### we check gtol -- nu=0.005, alpha=6.25e-05, hqhs=18-18, tol=0.0001'
print '$3.54e-04, 5.00e-04, 7.07e-04, 1.00e-03, 1.41e-03, 2.00e-03$'
wts = np.array([27.8415, 27.5140, 21.5160, 9.0790, 7.2650, 5.2316, 4.2893])
ovh = np.array([6.6551, 6.3508, 5.2010, 1.8914, 1.4314, 1.0682, 0.8951])
substractoverhead(wts, ovh)

print '### we check gtol -- nu=0.005, alpha=6.25e-05, hqhs=13-18, tol=0.0001'
print '$3.54e-04, 5.00e-04, 7.07e-04, 1.00e-03, 1.41e-03, 2.00e-03$'
wts = np.array([19.5168, 16.8355, 14.3263, 12.2472, 8.4389, 4.5464, 3.9862])
ovh = np.array([5.7955, 5.1725, 4.0873, 3.6299, 2.4444, 1.2576, 1.1042])
substractoverhead(wts, ovh)
