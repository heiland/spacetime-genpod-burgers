space-time-parameter Galerkin POD
---

Implementation of space-time-parameter generalized POD and space-time Galerkin for solving Burgers' equation for various diffusion parameters.

To get started run `python run_burger_timspapod.py` - this will give the approximation of a solution to the Burgers' using a space-time Galerkin scheme with POD optimized bases in space and time.

To rerun the numerical examples run `python run_numtests_.py`. Set `timingsonly = True` there, to only get the timings.

### Dependencies:
 * dolfin (FEniCS) (v 1.3.0; 1.5.0; 2016.2.0)
 * scipy (v 0.13.3; 0.17.0)
 * numpy (v 1.8.2; 1.11.0)
 * scikit-sparse (v 0.3)
    * used for factorization of the mass matrices
	* critical for critical parameters (like small `nu`)
    * scipy's, numpy's built-in Cholesky routines can be used instead but the algorithm may fail at critical parameters
 * and my home-brew python modules
   * [dolfin_navier_scipy](https://github.com/highlando/dolfin_navier_scipy) -- interface between `scipy` and `fenics`, a timer function, and caching of intermediate results
   * [mat-lib-plots](https://github.com/highlando/mat-lib-plots) -- routines for plotting
   * [sadptprj_riclyap_adi](https://github.com/highlando/sadptprj_riclyap_adi) -- linear algebra helper routines
 * the branch `gh-deps-included` has the home-brew python modules already included

### Notes on the Implementation
#### Initial Value
In the space time Galerkin scheme the initial value is treated like a Dirichlet boundary value. This means that we enforce that `s(0)=0` for all test functions `s`. This removes one *dof* from the test space or one equation from the system. The corresponding *dof* in the trial space is then eliminated using the initial condition so that the system is square again.

To achieve this in the reduced order model we proceed as follows:
 - We zero out all entries in the generalized measurement matrix that are associated with the *dof* in the time test space associated with `t=0`. In our implementation this is setting the first column to zero. 
 - Accordingly, the corresponding singular vectors all have a `0` as their first entry. 
 - Then we add `s0 = [1 0 0 ... 0]` as the first singular. Note that `kron(s0, iniv)` gives the *boundary values* in the space time discretization.
 - In the assembled reduced system, the equations that come from with `s0` are to be removed. In the given time space structure, this is using the reduced (by the first line) time mass matrices `rms = ms[1:, :]` in the kronecker product or removing the first `hny` lines in the nonlinear terms or rhs. 
 - From the linear parts like `apart = kron(rms, ared)` the spare *dofs* in the system are removed as follows: 
   - We consider `apart = kron(rms[:, 1:], ared)` (without the first columns)
   - and move the known values `kron(rms[:, :1], ared)*iniv` (the first columns times the initial value) to the rhs
 - In iterative schemes the initial value are considered with the current solution to evaluate the nonlinearities
 - For linearizations, we compute the full Jacobian and afterwards remove the first lines and columns

