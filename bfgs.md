approach
---
 
 * use scipy.optimize.fmin_bfgs with
   * f(u) -- eva(costfun(fwdprob(u), u))
   * Df(u) -- adjoint_problem
 * u ~ dimension k*Nts == reduced state * time disc

checks
---

 * function call overhead -- as implemented, the fwd sol for eva of the costfun is not recycled for the gradient
 * the nonlinearity...

todous
---

 * define: k == poddim and Nts
 * DONE: eva redcostfun -- with mass matrices
 * DONE: eva fwd integrate with scipy integrate and store only on Nts grid
   * interpolate curu
 * DONE: eva red adjoint equation
   * interpolate fwdsol
   * function that returns the reduced bwd system for a red fwdsol
 * function: compute the gradient for `ucur`
   * add feature `compgrad` to costfun
   * check: what mass mat for `lvec` is needed for the red cost fun gradient -- questionable:
	  * `MLk` ~ low-dim approximation of the gradient descend iteration
	  * `MVk` ~ optimization of the low-dim approximation
	  * we go with `MVk` -- the more that `MLk=MVk=I` 
 * somethings wrong with the gradient function -- bfgs with numerically computed gradients works. 
 * DONE: compare num gradient and lambda based -- problem only in terminal value -- less significant for more time steps

what else
---
 * other nonconstant targets -- heart shape :P
 * DEIM -- hope I can do without
